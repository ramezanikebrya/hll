package src.ca.NetSysLab.algo;
import src.ca.NetSysLab.util.RandomNumberUtil;

import java.nio.ByteBuffer;
import java.util.BitSet;
import java.security.*;


public class HyperLogLog {
    public double substreamsNum;
    public int zeroes;
    public int[] substreamsNumArray;
    public double baselineEstimate;
    public double alpha_m;

    public HyperLogLog(int substreamsNum) {
        this.substreamsNum = substreamsNum;
    }

    private static int logBase2(int N) {
        return (int) (Math.log(N) / Math.log(2));
    }

    public void initializeRegister(BitSet bitSet) {
        substreamsNumArray = new int[(int) substreamsNum];
        bitSet.stream().forEach(i -> {
            try {
                registerInt(i);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        });
    }

    public void replicateRandomNum(BitSet bitSet) {
        bitSet.stream().forEach(i -> {
            try {
                while (RandomNumberUtil.prob2Bool(0.1)){
                    registerInt(i);
                }
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        });
    }

    public void registerInt(int sequence) throws NoSuchAlgorithmException {
        int x = getHash(sequence);
        int j = getSubstreamIndex(sequence);
        int rho_x = rho(x);
        if (rho_x > substreamsNumArray[j]) {
            substreamsNumArray[j] = rho_x;
        }
    }

    public int getSubstreamIndex(int sequence) {
        int bitshift = 31 - logBase2((int) substreamsNum);
        return ((sequence * 0xbc164501) & 0x7fffffff) >> bitshift;
    }

    public int fromByteArray(byte[] bytes) {
        return ((bytes[0] & 0xFF) << 24) |
                ((bytes[1] & 0xFF) << 16) |
                ((bytes[2] & 0xFF) << 8 ) |
                ((bytes[3] & 0xFF) << 0 );
    }
    public int getHash(int v) throws NoSuchAlgorithmException {
        byte[] bytesOfMessage = ByteBuffer.allocate(4).putInt(v).array();
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.reset();
        md.update(bytesOfMessage);
        byte[] theMD5digest = md.digest();
        int hashValue = fromByteArray(theMD5digest);
        return hashValue;
    }

    public int rho(int w) {
        return Integer.numberOfLeadingZeros(w) + 1;
    }

    public double estimate() {
        zeroes = zerosNum();
        baselineEstimate = getHarmonicMean();
        if ((baselineEstimate <= (5.0 / 2.0) * substreamsNum) && zeroes > 0) {
            return substreamsNum * Math.log(substreamsNum / zeroes);
        }
        if (baselineEstimate > (1.0 / 30.0) * Math.pow(2.0, 32.0)) {
            baselineEstimate = Math.pow(-2.0, 32.0) * Math.log(1 - (baselineEstimate / Math.pow(2.0, 32.0)));
        }
        return baselineEstimate;
    }

    public int zerosNum() {
        int count = 0;
        for (int i = 0; i < substreamsNumArray.length; i++) {
            if (substreamsNumArray[i] == 0) {
                count++;
            }
        }
        return count;
    }

    public double getHarmonicMean() {
        alpha_m = getAlpha();
        double sum = 0;
        for (int i = 0; i < substreamsNumArray.length; i++) {
            sum += Math.pow(2.0, -substreamsNumArray[i]);
        }
        return (alpha_m * (Math.pow(substreamsNum, 2.0))) * Math.pow(sum, -1.0);
    }

    public double getAlpha() {
        return 0.7213 / (1.0 + (1.079 / substreamsNum));
    }
}