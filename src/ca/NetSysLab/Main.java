package src.ca.NetSysLab;

import src.ca.NetSysLab.util.Observation;
import src.ca.NetSysLab.util.Histogram;
import src.ca.NetSysLab.util.FileUtil;

import java.io.IOException;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws NumberFormatException, IOException {
//        int[] elementsNum = {10000, 100000, 10000000};
//        int[] elementsNum = {10000};
//        int[] substreamsNum = {64, 256, 1024, 4096};
//        int[] substreamsNum = {64};
        if (args.length != 3) {
            System.out.println("you need to give three arguments. First, the number of elements, second the number of substream length, third the number of runs");
            return;
        }
        int elementsNum = Integer.parseInt(args[0]);
        int substreamsNum = Integer.parseInt(args[1]);
        int numberOfRuns = Integer.parseInt(args[2]);
        ArrayList<Observation> observations = new ArrayList<>();
        Observation observation = new Observation(elementsNum, substreamsNum, numberOfRuns);
        observation.run();
        observations.add(observation);

//        for (int N: elementsNum){
//            for (int m: substreamsNum){
//                Observation observation = new Observation(N, m, 100);
//                observation.run();
//                observations.add(observation);
//            }
//        }
        FileUtil.exportLatex(observations, "results.tex");
        Histogram histogram = new Histogram(observations.get(0).getResults(), 10);
        histogram.toCSV("results/hist.csv");
    }
}