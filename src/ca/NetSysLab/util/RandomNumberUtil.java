package src.ca.NetSysLab.util;

import java.util.BitSet;
import java.util.Random;

public class RandomNumberUtil {
    /**
     * generate N distinct random input
     * @param elementsNum number of elements to be generated
     * @param seed pseudo-random seed
     * @return
     */
    public static BitSet generatedInput(int elementsNum, int seed) {
        BitSet bitSet = new BitSet(Integer.MAX_VALUE);
        Random rand = new Random(seed);
        int cardinality = 0;
        while (cardinality < elementsNum) {
            int v = rand.nextInt(Integer.MAX_VALUE);
            if (!bitSet.get(v)) {
                bitSet.set(v);
                cardinality++;
            }
        }
        return bitSet;
    }


    public static boolean prob2Bool(double probability) {
        return prob2Bool(probability, 0.001);
    }

    public static boolean prob2Bool(double probability, double precision){
        Random rand = new Random(0);
        return rand.nextInt((int) (1/precision)) < (int)(probability/precision);
    }
}