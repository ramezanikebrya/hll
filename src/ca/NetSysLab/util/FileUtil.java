package src.ca.NetSysLab.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class FileUtil {
    public static void exportLatex(ArrayList<Observation> utilities, String filename) {
        File f = new File("results/" + filename);
        try (PrintWriter pw = new PrintWriter(f)) {
            pw.println("\\begin{tabular}{rrrr}" + "\\");
            pw.println(" $n$ & $m$ & 1st sigma & 2nd sigma " + " \\\\");
            for (int i = 0; i < utilities.size(); ++i) {
                String[] fields = new String[]{Integer.toString(utilities.get(i).getElements_num()), Integer.toString(utilities.get(i).getSubstreams_num()),
                        Integer.toString(utilities.get(i).getSigma1()), Integer.toString(utilities.get(i).getSigma2())};
                pw.println(String.join(" & ", fields) + " \\\\ ");
            }
            pw.println("\\end{tabular}");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    public static void generatePlotGpi(double min, double max) {
        String filename = "plot.gpi";
        File f = new File("results/" + filename);
        try (PrintWriter pw = new PrintWriter(f)) {
            pw.println("set xlabel 'Estimated N' ");
            pw.println("set ylabel 'Occurances of Estimation' ");
            pw.println("n=10");
            pw.println("max="+max);
            pw.println("min="+min);
            pw.println("width=(max-min)/n");
            pw.println("set boxwidth width * 0.9");
            pw.println("set style fill solid 0.5");
            pw.println("set tics format \"%.1s%c\"");
            pw.println("set ytics 1");
            pw.println("plot 'results/hist.csv' using 1:3 smooth freq with boxes");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}