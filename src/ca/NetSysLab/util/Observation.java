package src.ca.NetSysLab.util;

import src.ca.NetSysLab.algo.HyperLogLog;

import java.util.BitSet;

public class Observation {
    private int elements_num;
    private int substreams_num;
    private double[] results;
    private int runsNum;
    private HyperLogLog hll;
    private final double error;
    private int sigma1;
    private int sigma2;

    public Observation(int elements_num, int substreams_num, int runsNum) {
        this.error = 1.04 / Math.sqrt(substreams_num);
        this.sigma1 = 0;
        this.sigma2 = 0;
        this.elements_num = elements_num;
        this.substreams_num = substreams_num;
        this.runsNum = runsNum;
        hll = new HyperLogLog(substreams_num);
        results = new double[runsNum];
    }

    public void run() throws NumberFormatException {
        for (int i = 0; i < runsNum; i++) {
            BitSet input = RandomNumberUtil.generatedInput(elements_num, i);
            hll.initializeRegister(input);
            hll.replicateRandomNum(input);
            double res = hll.estimate();

            //memory usage
//            Runtime rt = Runtime.getRuntime();
//            double prevTotal = 0;
//            double total = rt.totalMemory();
//            double free = rt.freeMemory();
//            double prevFree = rt.freeMemory();
//            if (total != prevTotal || free != prevFree) {
//                double used = total - free;
//                double prevUsed = (prevTotal - prevFree);
//                System.out.println(
//                        "#" +
//                                ", Total: " + total/1000000 + "M" +
//                                ", Used: " + used/1000000 + "M" +
//                                ", ∆Used: " + (used - prevUsed)/1000000 + "M" +
//                                ", Free: " + free/1000000 + "M" +
//                                ", ∆Free: " + (free - prevFree)/1000000 + "M") ;
//                prevTotal = total;
//                prevFree = free;
//            }

            results[i] = res;
            if ((double) elements_num * (1 - 1 * error) <= res && res <= (double) elements_num * (1 + 1 * error)) sigma1++;
            if ((double) elements_num * (1 - 2 * error) <= res && res <= (double) elements_num * (1 + 2 * error)) sigma2++;
        }
    }

    public int getElements_num() {
        return this.elements_num;
    }

    public int getSubstreams_num() {
        return this.substreams_num;
    }

    public double[] getResults() {
        return this.results;
    }

    public int getSigma1() {
        return this.sigma1;
    }

    public int getSigma2() {
        return this.sigma2;
    }
}