# README #

### Implementation of HyperLogLog algorithm ###
This repo contains the java implementation of HLL. 


### Dependencies ###

* Java 11

    $ sudo apt install openjdk-11-jdk
    
* GnuPlot

    $ sudo apt install gnuplot

### How to run the code ###
You can run the code through the jar file provided. The program gets three arguments, the number of distinct elements, the number of substreams, and the number of executions respectively. The script below is an example:

    $ git clone https://bitbucket.org/ramezanikebrya/hll/src/master/

	$ java -jar hll.jar 10000000 4096 100

	$ gnuplot -p results/plot.gpi

The last command in the above script will generate the histogram. 
